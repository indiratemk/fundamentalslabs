package com.example.indira.droidcafehomework;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class FirstChildActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_child);
    }
}

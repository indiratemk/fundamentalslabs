package com.example.indira.droidcafehomework;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void firstImageClicked(View view) {
        Intent intent = new Intent(this, FirstChildActivity.class);
        startActivity(intent);
    }

    public void secondImageClicked(View view) {
        Intent intent = new Intent(this, SecondChildActivity.class);
        startActivity(intent);
    }

    public void thirdImageClicked(View view) {
        Intent intent = new Intent(this, ThirdChildActivity.class);
        startActivity(intent);
    }
}

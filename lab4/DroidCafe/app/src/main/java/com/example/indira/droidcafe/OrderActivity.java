package com.example.indira.droidcafe;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class OrderActivity extends AppCompatActivity implements
            AdapterView.OnItemSelectedListener {

    private static final String TAG = "OrderActivity";
    private CheckBox juiceCheckBox;
    private CheckBox bananaCheckBox;
    private CheckBox waterCheckBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        Intent intent = getIntent();
        String message = "Order: " +
                intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        TextView textView = findViewById(R.id.order_textview);
        textView.setText(message);

        juiceCheckBox = findViewById(R.id.juice_check_box);
        bananaCheckBox = findViewById(R.id.banana_check_box);
        waterCheckBox = findViewById(R.id.water_check_box);

        Spinner spinner = findViewById(R.id.label_spinner);
        if (spinner != null) {
            spinner.setOnItemSelectedListener(this);
        }
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.labels_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);

        if (spinner != null) {
            spinner.setAdapter(adapter);
        }

        EditText editText = findViewById(R.id.phone_text);
        if (editText != null)
            editText.setOnEditorActionListener
                    (new TextView.OnEditorActionListener() {
                        @Override
                        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                            boolean handled = false;
                            if (actionId == EditorInfo.IME_ACTION_SEND) {
                                dialNumber();
                                handled = true;
                            }
                            return handled;
                        }

                        private void dialNumber() {
                            EditText editText = findViewById(R.id.phone_text);
                            String phoneNum = null;
                            if (editText != null) phoneNum = "tel:" + editText.getText().toString();
                            Log.d(TAG, "dialNumber: " + phoneNum);
                            Intent intent = new Intent(Intent.ACTION_DIAL);
                            intent.setData(Uri.parse(phoneNum));
                            if (intent.resolveActivity(getPackageManager()) != null) {
                                startActivity(intent);
                            } else {
                                Log.d("ImplicitIntents", "Can't handle this!");
                            }
                        }

                    });
    }

    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        switch (view.getId()) {
            case R.id.sameday:
                if (checked)
                    displayToast(getString(R.string.same_day_messenger_service));
                break;
            case R.id.nextday:
                if (checked)
                    displayToast(getString(R.string.next_day_ground_delivery));
                break;
            case R.id.pickup:
                if (checked)
                    displayToast(getString(R.string.pick_up));
                break;
            default:
                break;
        }
    }

    public void displayToast(String message) {
        Toast.makeText(getApplicationContext(), message,
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String spinnerLabel = parent.getItemAtPosition(position).toString();
        displayToast(spinnerLabel);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void showToppings(View view) {
        String toppings = "Toppings: ";
        if (juiceCheckBox.isChecked()) {
            toppings += "juice ";
        }
        if (bananaCheckBox.isChecked()) {
            toppings += "banana ";
        }
        if (waterCheckBox.isChecked()) {
            toppings += "water ";
        }
        displayToast(toppings);
    }


    public void showDatePicker(View view) {
        DialogFragment dialogFragment = new OrderDatePickerFragment();
        dialogFragment.show(getSupportFragmentManager(), "OrderDatePicker");
    }

    public void processDate(int year, int month, int day) {
        String year_string = Integer.toString(year);
        String month_string = Integer.toString(month + 1);
        String day_string = Integer.toString(day);
        String dateMessage = day_string + "." + month_string + "." + year_string;
        Toast.makeText(this,
                getString(R.string.date) + dateMessage, Toast.LENGTH_SHORT).show();
    }
}

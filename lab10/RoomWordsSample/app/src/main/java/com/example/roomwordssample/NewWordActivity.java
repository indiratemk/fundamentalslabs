package com.example.roomwordssample;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class NewWordActivity extends AppCompatActivity {

    public static final String EXTRA_REPLY =
            "com.example.android.roomwordssample.REPLY";
    public static final String EXTRA_ID =
            "com.example.android.roomwordssample.ID";

//    private SharedPreferences mSharedPreferences;
//    private String sharedPrefFile = "com.example.roomwordssample";

    private EditText mEditWordView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_word);

//        mSharedPreferences = getSharedPreferences(sharedPrefFile, MODE_PRIVATE);

        mEditWordView = findViewById(R.id.edit_word);

        final Intent intent = getIntent();
        if (intent != null) {
            mEditWordView.setText(intent.getStringExtra(MainActivity.EXTRA_WORD_TEXT));
        }

        final Button button = findViewById(R.id.button_save);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent replyIntent = new Intent();
                if (TextUtils.isEmpty(mEditWordView.getText())) {
                    setResult(RESULT_CANCELED, replyIntent);
                } else {
                    String word = mEditWordView.getText().toString();
                    replyIntent.putExtra(EXTRA_REPLY, word);
                    if (intent.hasExtra(MainActivity.EXTRA_WORD_ID)) {
                        int id = intent.getIntExtra(MainActivity.EXTRA_WORD_ID,  -1);
                        replyIntent.putExtra(EXTRA_ID, id);
                    }
                    setResult(RESULT_OK, replyIntent);
                }

//                SharedPreferences.Editor preferencesEditor = mSharedPreferences.edit();
//                preferencesEditor.putString(EXTRA_REPLY, mEditWordView.getText().toString());
//                setResult(RESULT_OK, replyIntent);
                finish();
            }
        });
    }
}

package com.example.hellosharedprefs;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class SettingsActivity extends AppCompatActivity {
    private String sharedPrefFile = "com.example.hellosharedpref";

    private Intent mIntent;
    private SharedPreferences mPreferences;


    private final String COUNT_KEY = "count";

    private final String COLOR_KEY = "color";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        mIntent = getIntent();

        mPreferences = getSharedPreferences(sharedPrefFile, MODE_PRIVATE);
    }

    public void resetPreferences(View view) {
        SharedPreferences.Editor preferencesEditor = mPreferences.edit();
        preferencesEditor.clear();
        preferencesEditor.apply();
    }

    public void savePreferences(View view) {
        SharedPreferences.Editor preferencesEditor = mPreferences.edit();
        preferencesEditor.putInt(COUNT_KEY, mIntent.getIntExtra(COUNT_KEY, 0));
        preferencesEditor.putInt(COLOR_KEY, mIntent.getIntExtra(COLOR_KEY,
                ContextCompat.getColor(this, R.color.default_background)));
        preferencesEditor.apply();
    }
}

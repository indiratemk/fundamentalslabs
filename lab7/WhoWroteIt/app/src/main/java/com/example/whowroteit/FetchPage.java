package com.example.whowroteit;

import android.os.AsyncTask;
import android.widget.TextView;

import java.lang.ref.WeakReference;

public class FetchPage extends AsyncTask<String, Void, String> {
    private WeakReference<TextView> mTitleText;

    FetchPage(TextView titleText) {
        this.mTitleText = new WeakReference<>(titleText);
    }

    @Override
    protected String doInBackground(String... strings) {
        return NetworkUtils.getPage(strings[0]);
    }

    @Override
    protected void onPostExecute(String s) {
        mTitleText.get().setText(s);
    }
}

package com.example.indira.scorekeeper;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class BatteryActivity extends AppCompatActivity {

    private int level = 0;
    private ImageView batteryImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_battery);

        batteryImageView = findViewById(R.id.battery);
    }

    public void increaseBattery(View view) {
        if (level < 6) {
            ++level;
            batteryImageView.setImageLevel(level);
        }
    }

    public void decreaseBattery(View view) {
        if (level > 0) {
            --level;
            batteryImageView.setImageLevel(level);
        }
    }
}
